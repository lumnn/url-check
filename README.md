# URL Checker

> A simple CLI tool to batch compare paths across 2 domains.

At the moment following is supported:

- Status code comparasion
- Redirect detection
- Title differences
- Meta description differences

## Installation

1. Clone repo
2. `npm install`
3. `npm run build`

## Usage

Most up to date description will be provided by the command itself:

`./dist/bin.js check --help`

For example

`./dist/bin.js check example.org test.example.org /path1 /path2`
