import fetch from 'node-fetch'
import { HtmlExtractor } from './HtmlExtractor.js'

type CheckResult = {
  matching: number,
  matchingKeys: {
    [prop: string]: number
  },
  total: number,
  paths: {
    [path: string]: PathResult
  }
}

type PathResult = {
  [domain: string]: DomainResult
}

type DomainResult = {
  status?: number,
  redirectCode?: number,
  redirectUrl?: string,
  title?: string|null,
  metaDescription?: string|null
}

const comparators = {
  title(a: string, b: string) {
    return a.toLowerCase().replaceAll(/\s+/g, '') === b.toLowerCase().replaceAll(/\s+/g, '')
  },

  metaDescription(a: string, b: string) {
    return a.toLowerCase().replaceAll(/\s+/g, '') === b.toLowerCase().replaceAll(/\s+/g, '')
  }
}

export async function check(paths: string[], domains: string[]): Promise<CheckResult> {
  paths = paths.map(path => {
    if (path.charAt(0) !== '/') {
      path = "/" + path
    }

    return path
  })

  domains = domains.map(domain => {
    if (!domain.startsWith('http://') || !domain.startsWith('http://')) {
      domain = "https://" + domain
    }

    if (domain.charAt(domain.length - 1) === "/") {
      domain = domain.substring(domain.length - 1, 1)
    }

    return domain
  })

  const result: CheckResult = {
    total: paths.length,
    matching: 0,
    matchingKeys: {},
    paths: {},
  }

  for (let path of paths) {
    process.stderr.write(path + "\n")

    const pathResults: PathResult = {}
    let compareAgainst: null|DomainResult = null

    for (let domain of domains) {
      const domainResult: DomainResult = {}

      let response = await fetch(domain + path, {
        redirect: "manual"
      })

      if (response.status === 301 || response.status === 302) {
        domainResult.redirectCode = response.status
        domainResult.redirectUrl = response.headers.get('Location') as string

        response = await fetch(domain + path)
      }

      domainResult.status = response.status
      const body = await response.text()

      const htmlExtractor = new HtmlExtractor(body)

      domainResult.title = htmlExtractor.title()
      domainResult.metaDescription = htmlExtractor.metaDescription()

      if (compareAgainst) {
        for (const key in domainResult) {
          if (compareAgainst[key] === domainResult[key]
            || (comparators[key] && comparators[key](compareAgainst[key], domainResult[key]))
          ) {
            delete domainResult[key]

            if (!result.matchingKeys[key]) {
              result.matchingKeys[key] = 1
            } else {
              result.matchingKeys[key]++
            }
          }
        }
      }

      pathResults[domain] = domainResult

      if (!compareAgainst) {
        compareAgainst = domainResult
      }
    }

    let matching = true
    for (const domainResult of Object.values(pathResults)) {
      if (domainResult === compareAgainst) {
        continue
      }

      if (Object.keys(domainResult).length === 0) {
        continue
      }

      matching = false
      break
    }

    result.paths[path] = pathResults

    if (matching) {
      result.matching++
    }
  }

  return result
}
