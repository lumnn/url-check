#!/usr/bin/env node

import yargs from 'yargs'
import { hideBin } from 'yargs/helpers'
import { check } from './check.js'

const args = yargs(hideBin(process.argv))
  .command(
    'check [domain1] [domain2] [paths...]',
    'Compare status code against domain for same path',
    (yargs) => {
      yargs.positional('domain1', {
        type: 'string',
        describe: 'domain to compare against'
      })
      yargs.positional('domain2', {
        type: 'string',
        describe: 'compared domain'
      })
      yargs.positional('paths', {
        type: 'string',
        describe: 'paths on each domain to check'
      })

    },
    async function (argv) {
      const paths = (argv.paths as string[]).map(path => {
        if (path.charAt(0) !== '/') {
          path = "/" + path
        }

        return path
      })

      const domain1 = argv.domain1 as string
      const domain2 = argv.domain2 as string

      const result = await check(paths, [domain1, domain2])

      process.stdout.write(JSON.stringify(result, null, 2))
    }
  )
  .parseAsync()
