import { decodeEntity } from 'html-entities';
import { HTMLElement, parse } from 'node-html-parser';

export class HtmlExtractor {
  html: string
  parser: HTMLElement|null = null

  constructor (html: string) {
    this.html = html
  }

  getParser() {
    if (!this.parser) {
      this.parser = parse(this.html)
    }

    return this.parser
  }

  title (): null|string {
    const titleTag = this.getParser().getElementsByTagName('title')

    if (titleTag[0]) {
      return decodeEntity(titleTag[0].innerText.trim())
    }

    return null
  }

  metaDescription (): null|string {
    const descriptionTag = this.getParser().querySelector('meta[name="description"]')

    if (!descriptionTag) {
      return null
    }

    let description = descriptionTag.getAttribute('content')?.trim()

    if (!description) {
      return null
    }

    return decodeEntity(description)
  }
}
